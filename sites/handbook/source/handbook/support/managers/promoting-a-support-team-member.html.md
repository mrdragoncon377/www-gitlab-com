---
layout: handbook-page-toc
title: Promoting a Support Engineer
description: "How to handle Support Engineer promotions"
---

### Support Engineer Promotion Process for Support Managers - Quick Guide

The companywide People Group process should be followed [For Managers: Requesting a Promotion or Compensation Change](/handbook/people-group/promotions-transfers/#for-managers-requesting-a-promotion-or-compensation-change). The steps below are an addendum for notification and review for Support Engineering managers taking into account the Customer Support Department's organisational structure and is not meant as a replacement for any steps in the companywide people group process.

#### Planning

[At GitLab, we promote on a quarterly cadence](handbook/people-group/promotions-transfers/#promotion-process--timeline). Once a quarter, review
your reports progress towards promotion and work with your manager to ensure the future promotion is accounted for in the annual plan.

If you have any questions, please use `#fy23_support_promotions`.

#### Calibration

While the [calibration section of the handbook](/handbook/people-group/promotions-transfers/#calibration) suggests a quarter cadence, in Support we review
promotion docs asynchronously as soon as they are ready for review.

* Complete Path to Promotion Document with Support Engineer.
* Create a public Google Docs view link for everyone at Gitlab to allow sharing of the promotion document which will be used for promotion approval process.
* Discuss the promotion candidate with your manager.
* Make a copy of the promotion document and share it in the confidential doc used in the cross-regional sync with Support Managers (ensure Support Managers can Edit).
* Gather feedback on the promotion document on the private copy shared with the Support Managers.
* If necessary, update the public document based on feedback from Support Managers.
* Get the approval to proceed from your manager.

#### Submission
* [Submit a Promotion request in BambooHR](/handbook/people-group/promotions-transfers/#submit-a-promotion-request-in-bamboohr)

#### Announcement

* Once approval has been received notify the engineer.
* Send a message to the `#team-member-updates` Slack channel.
* Share the message from `#team-member-updates` into the `#support-team-chat` channel for Support Team visibility.

#### Day of transfer

On the day of transfer, there are a lot of tasks that need to be done. To
simplify the process, we recommend using the
[Support Member Transfer issue template](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/-/issues/new?issuable_template=Support%20Member%20Transfer)
to generate an issue in the
[Support Ops Project](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project)
issue tracker. This will guide you through the process.
